using System;
using System.Collections.Generic;
using UnityEngine;
using Fusion;

public class Spawner : MonoBehaviour
{
    [Header("Components")]
    [SerializeField]
    private Transform objectsParent;
    [SerializeField]
    private GenericObject objectPrefab;

    [Header("Settings")]
    [SerializeField]
    private uint objectsAmount;
    [SerializeField]
    [Range(0, 10)]
    private float height = 8;

    private readonly List<GenericObject> objects = new List<GenericObject>();

    private NetworkRunner runner;

    #region Init

    public void Init(NetworkRunner runner)
    {
        if (runner.LocalPlayer.PlayerId > 1) return;

        this.runner = runner;

        SpawnObjects();
        InitObjects();
    }

    private void SpawnObjects()
    {
        Vector2Int divisors = GetSpawnDivisors();
        Vector2 pivot = (divisors - Vector2.one) / 2;
        float separation = height / divisors.y;

        int count = 0;

        for (int x = 0; x < divisors.x; x++)
        {
            for (int y = 0; y < divisors.y; y++)
            {
                Vector2 position = (new Vector2(x, y) - pivot) * separation;

                SpawnObject(position, separation);

                if (++count >= objectsAmount) return;
            }
        }
    }

    private void SpawnObject(Vector3 position, float separation)
    {
        GenericObject obj = runner.Spawn(objectPrefab, position);

        obj.name = objectPrefab.name;
        obj.transform.SetParent(objectsParent);

        obj.Init(separation);

        objects.Add(obj);
    }

    private void InitObjects()
    {
        foreach (GenericObject obj in objects)
        {
            obj.NetworkInit();
        }
    }

    #endregion

    // --------------------------------------------------------------------------------------------------------------------------------

    #region Other

    private Vector2Int GetSpawnDivisors()
    {
        int sqrt = Mathf.RoundToInt(Mathf.Sqrt(objectsAmount));

        int a = GetLowerDivisor();
        int b = GetCodivisor(a);

        return new Vector2Int(b, a);

        int GetLowerDivisor()
        {
            const float maxRatio = 3;

            for (int i = sqrt; i > 1; i--)
            {
                bool isDivisor = objectsAmount % i == 0;
                bool isBelowMaxRatio = (float)GetCodivisor(i) / i <= maxRatio;

                if (isDivisor && isBelowMaxRatio)
                {
                    return i;
                }
            }

            return sqrt;
        }

        int GetCodivisor(int d) => Mathf.CeilToInt((float)objectsAmount / d);
    }

    #endregion

}