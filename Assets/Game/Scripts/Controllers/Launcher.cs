using System.Collections.Generic;
using System;
using UnityEngine;
using Fusion;
using Fusion.Sockets;

public class Launcher : MonoBehaviour, INetworkRunnerCallbacks
{
    [SerializeField]
    private NetworkRunner runner;

    private const string roomName = "Room";

    #region StartGame

    private void Start()
    {
        StartGame();
    }

    private async void StartGame()
    {
        NetworkSceneInfo scene = new NetworkSceneInfo();
        scene.AddSceneRef(SceneRef.FromIndex(1));

        StartGameArgs args = new StartGameArgs()
        {
            SessionName = roomName,
            GameMode = GameMode.Shared,
            Scene = scene,
        };

        StartGameResult result = await runner.StartGame(args);

        Debug.Log($"StartGame: {(result.Ok ? "Success" : "Failed")}");
    }

    private void OnSceneLoad()
    {
        Spawner spawner = FindObjectOfType<Spawner>();

        if (spawner == null) throw new Exception("Spawner not found");

        spawner.Init(runner);
    }

    #endregion

    // --------------------------------------------------------------------------------------------------------------------------------

    #region INetworkRunnerCallbacks

    public void OnObjectExitAOI(NetworkRunner runner, NetworkObject obj, PlayerRef player) { }

    public void OnObjectEnterAOI(NetworkRunner runner, NetworkObject obj, PlayerRef player) { }

    public void OnPlayerJoined(NetworkRunner runner, PlayerRef player) { }

    public void OnPlayerLeft(NetworkRunner runner, PlayerRef player) { }

    public void OnInput(NetworkRunner runner, NetworkInput input) { }

    public void OnInputMissing(NetworkRunner runner, PlayerRef player, NetworkInput input) { }

    public void OnShutdown(NetworkRunner runner, ShutdownReason shutdownReason) { }

    public void OnConnectedToServer(NetworkRunner runner) { }

    public void OnDisconnectedFromServer(NetworkRunner runner, NetDisconnectReason reason) { }

    public void OnConnectRequest(NetworkRunner runner, NetworkRunnerCallbackArgs.ConnectRequest request, byte[] token) { }

    public void OnConnectFailed(NetworkRunner runner, NetAddress remoteAddress, NetConnectFailedReason reason) { }

    public void OnUserSimulationMessage(NetworkRunner runner, SimulationMessagePtr message) { }

    public void OnSessionListUpdated(NetworkRunner runner, List<SessionInfo> sessionList) { }

    public void OnCustomAuthenticationResponse(NetworkRunner runner, Dictionary<string, object> data) { }

    public void OnHostMigration(NetworkRunner runner, HostMigrationToken hostMigrationToken) { }

    public void OnReliableDataReceived(NetworkRunner runner, PlayerRef player, ReliableKey key, ArraySegment<byte> data) { }

    public void OnReliableDataProgress(NetworkRunner runner, PlayerRef player, ReliableKey key, float progress) { }

    public void OnSceneLoadDone(NetworkRunner runner) => OnSceneLoad();

    public void OnSceneLoadStart(NetworkRunner runner) { }

    #endregion

}