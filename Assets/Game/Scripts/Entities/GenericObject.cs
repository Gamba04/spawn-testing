using UnityEngine;
using Fusion;

public class GenericObject : NetworkBehaviour
{

    #region Custom Data

    private struct NetworkData : INetworkStruct
    {
        private byte a;
        private byte b;
        private byte c;

        public NetworkData(byte value)
        {
            a = value;
            b = value;
            c = value;
        }
    }

    #endregion

    [SerializeField]
    [Range(0, 1)]
    private float size = 0.5f;

    [Networked]
    private NetworkData networkData { get; set; }

    public override void Spawned()
    {
        Debug.Log("Spawned");
    }

    public void Init(float separation)
    {
        transform.localScale = Vector3.one * (size * separation);
    }

    public void NetworkInit()
    {
        networkData = new NetworkData(1);
    }
}